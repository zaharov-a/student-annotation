<?php

namespace Ox3a\Annotation;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use ReflectionClass;
use ReflectionException;


class Analyzer
{

    /**
     * @param string $className
     * @return array
     * @throws ReflectionException
     * @throws AnnotationException
     */
    public function run($className)
    {
        AnnotationRegistry::registerLoader('class_exists');

        $class = new ReflectionClass($className);

        $reader = new AnnotationReader();

        $methods = $class->getMethods();

        $result = [];

        foreach ($methods as $method) {
            $annotations = $reader->getMethodAnnotations($method);

            $result[$method->getName()] = [];
            foreach ($annotations as $annotation) {
                $annotationShortClass = (new ReflectionClass($annotation))->getShortName();
                if ($annotation instanceof IStudentAnnotation) {
                    if (!isset($result[$method->getName()][$annotationShortClass])) {
                        $result[$method->getName()][$annotationShortClass] = [];
                    }
                    $result[$method->getName()][$annotationShortClass][] = $annotation->compile();
                }
            }
        }

        return $result;
    }


}
