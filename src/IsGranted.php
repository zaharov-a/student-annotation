<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      04.09.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\Annotation;


/**
 * Class IsGranted
 * @Annotation
 * @package Ox3a\Annotation
 */
class IsGranted implements IStudentAnnotation
{
    /**
     * полностью авторизованный пользователь
     */
    const  IS_AUTHENTICATED_FULLY = 1;

    /**
     * неавторизованный пользователь
     */
    const  IS_ANONYMOUS = 2;

    protected $type;


    /**
     * IsGranted constructor.
     * @param array
     */
    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['type'] = $data['value'];
            unset($data['value']);
        }
        $this->setType($data['type']);
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @param mixed $type
     * @return IsGranted
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    public function compile()
    {
        return [
            'type' => $this->getType(),
        ];
    }


}
