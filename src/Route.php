<?php


namespace Ox3a\Annotation;


use BadMethodCallException;
use function get_class;

/**
 * Class Route
 * @Annotation
 * @package Ox3a\Annotation
 */
final class Route implements IStudentAnnotation
{

    protected $name;

    protected $defaults = [];

    protected $constraints = [];

    protected $route = '';


    public function __construct($data)
    {

        if (isset($data['value'])) {
            $data['path'] = $data['value'];
            unset($data['value']);
        }

        $this->setPath($data['path']);
        unset($data['path']);

        foreach ($data as $key => $value) {
            $method = 'set' . str_replace('_', '', $key);
            if (!method_exists($this, $method)) {
                throw new BadMethodCallException(sprintf('Unknown property "%s" on annotation "%s".', $key, get_class($this)));
            }
            $this->$method($value);
        }
    }


    /**
     * @param string $path
     * @return Route
     */
    public function setPath($path)
    {
        $data = $this->parsePath($path);

        $this->route       = $data['route'];
        $this->constraints = $data['constraints'];
        $this->defaults    = $data['defaults'];

        return $this;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param mixed $name
     * @return Route
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getDefaults()
    {
        return $this->defaults;
    }


    /**
     * @param array $defaults
     * @return Route
     */
    public function setDefaults($defaults)
    {
        if (is_array($defaults)) {
            $this->defaults = $defaults + $this->defaults;
        }
        return $this;
    }


    /**
     * @return array
     */
    public function getConstraints()
    {
        return $this->constraints;
    }


    public function setRequirements($requirements)
    {
        if (is_array($requirements)) {
            $this->constraints = $requirements + $this->constraints;
        }
        return $this;
    }


    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }


    public function compile()
    {
        return [
            'name'   => $this->getName(),
            'params' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => $this->getRoute(),
                    'constraints' => $this->getConstraints(),
                    'defaults'    => $this->getDefaults(),
                ],
            ],
        ];
    }


    public function parsePath($path)
    {
        $parts = array_map([$this, 'parsePathParts'], explode('/', $path));

        $route       = [];
        $defaults    = [];
        $constraints = [];

        foreach ($parts as $part) {
            if (is_string($part)) {
                $route[] = '/' . $part;
            } else {

                if (!is_null($part['requirements'])) {
                    $constraints[$part['var']] = $part['requirements'];
                }

                if (!is_null($part['default'])) {
                    if ($part['default'] !== '') {
                        $defaults[$part['var']] = $part['default'];
                    }

                    $route[] = '[/[:' . $part['var'] . ']]';
                } else {
                    $route[] = '/:' . $part['var'];
                }

            }
        }

        return [
            'route'       => substr(implode('', $route), 1),
            'constraints' => $constraints,
            'defaults'    => $defaults,
        ];
    }


    public function parsePathParts($pathPart)
    {
        if (preg_match('/^{(.+)}$/', $pathPart, $m)) {
            $var          = $m[1];
            $default      = null;
            $requirements = null;

            if (preg_match('/^([^<?]+)(<(.+)>)?(\?{1}(.{0,}))?$/', $var, $m)) {
                $var          = $m[1];
                $requirements = isset($m[3]) && $m[3] !== '' ? $m[3] : null;
                $default      = isset($m[5]) ? $m[5] : null;
            }

            return [
                'var'          => $var,
                'requirements' => $requirements,
                'default'      => $default,
            ];
        }
        return $pathPart;
    }

}
