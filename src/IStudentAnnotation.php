<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      04.09.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\Annotation;


interface IStudentAnnotation
{
    /**
     * @return array
     */
    public function compile();
}
