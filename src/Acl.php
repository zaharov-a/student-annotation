<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      04.09.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\Annotation;


/**
 * Class Acl
 * @Annotation
 * @package Ox3a\Annotation
 */
class Acl implements IStudentAnnotation
{
    protected $path;

    protected $expected;


    /**
     * Acl constructor.
     * @param $data
     */
    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['path'] = $data['value'];
            unset($data['value']);
        }

        $this->setPath($data['path']);

        if (isset($data['expected'])) {
            $this->setExpected($data['expected']);
        }
    }


    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * @param mixed $path
     * @return Acl
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getExpected()
    {
        return $this->expected;
    }


    /**
     * @param mixed $expected
     * @return Acl
     */
    public function setExpected($expected)
    {
        $this->expected = $expected;
        return $this;
    }


    public function compile()
    {
        return [
            'path'     => $this->getPath(),
            'expected' => $this->getExpected(),
        ];
    }


}
