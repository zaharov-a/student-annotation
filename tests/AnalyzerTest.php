<?php


require_once 'Controller.php';

use Doctrine\Common\Annotations\AnnotationException;
use Ox3a\Annotation\IsGranted;
use PHPUnit\Framework\TestCase;
use Ox3a\Annotation\Analyzer;

class AnalyzerTest extends TestCase
{
    /**
     * @throws ReflectionException
     * @throws AnnotationException
     */
    public function test1()
    {
        $className = 'Controller';

        $analyzer = new Analyzer();

        $result = $analyzer->run($className);

        $expected = [
            'defaultsAction' => [
                'Route' => [
                    [
                        'name'   => 'defaults',
                        'params' => [
                            'type'    => 'segment',
                            'options' => [
                                'route'       => '/:id/:action',
                                'constraints' => [
                                    'id' => '\d+',
                                ],
                                'defaults'    => [
                                    'action' => 'show',
                                ],
                            ],
                        ],
                    ],
                    [
                        'name'   => 'double',
                        'params' => [
                            'type'    => 'segment',
                            'options' => [
                                'route'       => '/:id/double',
                                'constraints' => [
                                    'id' => '\d+',
                                ],
                                'defaults'    => [],
                            ],
                        ],
                    ],
                ],
                'Acl'   => [
                    [
                        'path'     => 'root>access',
                        'expected' => 4,
                    ],
                ],
            ],
            'indexAction'    => [
                'Route' => [
                    [
                        'name'   => 'index',
                        'params' => [
                            'type'    => 'segment',
                            'options' => [
                                'route'       => '/',
                                'constraints' => [],
                                'defaults'    => [],
                            ],
                        ],
                    ],
                ],
            ],
            'paramsAction'   => [
                'Route'     => [
                    [
                        'name'   => 'params',
                        'params' => [
                            'type'    => 'segment',
                            'options' => [
                                'route'       => '/:id/edit',
                                'constraints' => [
                                    'id' => '\d+',
                                ],
                                'defaults'    => [],
                            ],
                        ],
                    ],
                ],
                'IsGranted' => [
                    [
                        'type' => IsGranted::IS_AUTHENTICATED_FULLY,
                    ],
                ],
            ],
        ];

        $this->assertEquals($result, $expected);
    }
}
