<?php

use Ox3a\Annotation\Route;
use Ox3a\Annotation\IsGranted;
use Ox3a\Annotation\Acl;

class Controller
{


    /**
     * @Route("/{id<\d+>}/{action}", name="defaults", defaults={"action": "show"})
     * @Route("/{id<\d+>}/double", name="double")
     * @Acl("root>access", expected="4")
     */
    public function defaultsAction()
    {
    }


    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
    }


    /**
     * @Route("/{id<\d+>}/edit", name="params")
     * @IsGranted(IsGranted::IS_AUTHENTICATED_FULLY)
     */
    public function paramsAction()
    {
    }
}
