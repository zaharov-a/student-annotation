<?php

use PHPUnit\Framework\TestCase;
use Ox3a\Annotation\Route;

class RouteTest extends TestCase
{

    /**
     * @param      $path
     * @param      $route
     * @param      $constraints
     * @param null $defaults
     * @dataProvider dataProviderPaths
     */
    public function testPath($path, $route, $constraints, $defaults = [])
    {
        $annotationRoute = new Route([
            'path' => $path,
        ]);

        $this->assertEquals(
            [$route, $constraints, $defaults],
            [$annotationRoute->getRoute(), $annotationRoute->getConstraints(), $annotationRoute->getDefaults()],
            $path
        );
    }


    public function dataProviderPaths()
    {
        // https://symfony.com/doc/current/routing.html#optional-parameters
        return [
            ["/blog/{page<0>}", "/blog/:page", ['page'=>'0'], []],
            ["/blog/{page?0}", "/blog[/[:page]]", [], ['page' => '0']],
            ["/blog/{page?}", "/blog[/[:page]]", [], []],
            ["/blog/{page??}", "/blog[/[:page]]", [], ['page' => '?']],
            ["/{id<\d+>}", "/:id", ['id' => "\d+"]],
            ["/blog/{page<\w?>??}", "/blog[/[:page]]", ['page' => '\w?'], ['page' => '?']],
            ["/{id<\d+>}/edit", "/:id/edit", ['id' => "\d+"]],
            ["/", "/", []],
            ["/rss.xml", "/rss.xml", []],
            ["/page/{page<[1-9]\d*>}", "/page/:page", ['page' => "[1-9]\d*"]],
            ["/posts/{slug}", "/posts/:slug", []],
            ["/posts/{slug}/{slug2}", "/posts/:slug/:slug2", []],
            ["/{id<\d+>}/{action}", "/:id/:action", ['id' => '\d+']],
            // ['"/blog/{page}", name="blog_list", requirements={"page"="\d+"}', "/blog/:page", ['page' => '\d+']],
            // ['"/blog/{page}", name="blog_list"', "/blog[/:page]", ['page' => '\d+']],
        ];
    }

}
